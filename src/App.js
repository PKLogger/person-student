import React, {useState} from "react";
import './App.css';
import Person from "./views/Person-Student/Person"
import ProtableExample from "./views/ProtableExample";
import AdminAntDesign from "./component/admin/AdminAntDesign";
import AdminBootstrap from './component/admin/AdminBootstrap';
import AdminReact from './component/admin/AdminReact';
function App(props) {
  
  return (
    <>
        {/* <Person/> */}
        {/* <ProtableExample/> */}
        {/* <AdminAntDesign/> */}
        {/* <AdminBootstrap /> */}
        <AdminReact/>
    </>
  );
}

export default App;
