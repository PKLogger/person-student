import React, { useEffect, useState } from 'react';
import dataService from '../../network/dataService';

function Time(props) {
    const [count, setCount] = useState(0);
    const [data, setData] = useState();
    useEffect(() => {
        const getBanner = async () => {
            let result = await dataService.getListBanner()
            setData(result.data)
        }
        getBanner()
    }, [])
    return (
        <>
            <div>    
                <p>You clicked {count} times</p>
                <button onClick={() => setCount(count + 1)}> Click me </button>
                <div>
                    {data ? data.map((item, index) => {
                        return  <div key={index}>{item.id}</div>
                    }):null}
                </div> 
                <div>
                    {data ? data.map((item, index) => {
                        let my_timestamp = item.createdAt;
                        var time = new Date(my_timestamp);

                        var t = Number(time);
                        return <div key={index}>{item.createdAt + " - " + time.getDate() +"/" + 
                        time.getMonth() + "/" + time.getFullYear() + " - " +
                        t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " - " + t.toLocaleString()}</div>
                    }):null}
                </div>
            </div>
            
        </>
    )
}
export default Time;