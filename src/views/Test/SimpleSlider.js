import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function SimpleSlider(props) {
    var state = {
        display: true,
        width: 480
    };
    var settings = {
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 2000,
        cssEase: "linear"

    };
    return (
        <div className="container">
            <h2> Single Item</h2> 
            <Slider {...settings}>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/FPT.png'} alt="FPT"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/Apollo.png'} alt="Apollo"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/EVERSAFE.png'} alt="EVERSAFE"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/ARDENT.png'} alt="ARDENT"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/Goulds Pumps.png'} alt="Goulds Pumps"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/LICOGI18.png'} alt="LICOGI18"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/PCC1.png'} alt="PCC1"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/Tecofi.png'} alt="Tecofi"/>
                </div>
                <div>
                    <img src={process.env.PUBLIC_URL + '/images/Petrolimex.png'} alt="Petrolimex"/>
                </div>
            </Slider>
        </div>
    );
}
export default SimpleSlider;