import React, {useState} from "react";
// import './App.css';
import Student from "./Student";

function Person(props) {
  const [count, setCount] = useState(0);
  return (
    <>
      <div className="container">
        <Student birth="15" id="1" >HFS</Student>;
        <Student birth="2353" id="4" >HFSOHYS</Student>;
        
        <div className="count-number-of-students" style={{display: "inline-flex"}}>
          <button className="btn btn-danger" onClick={() => setCount(count + 1)}>Click here</button>
          <h1 style={{marginLeft: "40px"}}>Page has {count} Student</h1>
        </div>
      </div>
    </>
  );
}

export default Person;
