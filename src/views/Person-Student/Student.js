import React, { useState } from "react";

function Student (props){
	return (
		<>
			<h1>{props.children}: Birth({props.birth}) + ID({props.id})</h1>
		</>
	);	
}
export default Student;