import React, { useEffect, useState } from 'react';
// import ProTable, { ProColumns } from '@ant-design/pro-table';
import ProTable, { viVNIntl, IntlProvider } from "@bmstravel/pro-table";
import { Input, Button } from 'antd';
import "./TableList.less";
import dataService from '../../networks/dataService';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    copyable: true,
  },
  {
    title: 'Age',
    dataIndex: 'age',
  },
  {
    title: 'date',
    dataIndex: 'date',
    valueType: 'date',
  },
  {
    title: 'option',
    valueType: 'option',
    dataIndex: 'id',
    render: (text, row, index, action) => [
      <a
        onClick={() => {
          window.alert('确认删除？');
          action.reload();
        }}
      >
        Delete
      </a>,
      <a
        onClick={() => {
          window.alert('确认刷新？');
          action.reload();
        }}
      >
        Reload
      </a>,
    ],
  },
];

function ProtableExample () {
  const [keywords, setKeywords] = useState('');
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [date, setDate] = useState('');
  const showProfile = async () => {
    try {
        let result = await dataService.displayProfile();
        if (result.code === 0) {
          setName(result.data.name)
          setAge(result.data.age)
          setDate(result.data.date)
        }
    } catch (err) { }
  }
  useEffect(() => {
    showProfile()
  }, [])

  return (
    <div className="container">
      <div>
        <ProTable //<{}, { keywords: string }>
          type="table" search={true} size="small" columns={columns} headerTitle="Danh sách"
          scrollTop={true}
          request={() => ({
            // data: [{name: 'Jack', age: 12, date: '2020-01-02', idPassport: '000',},],
            data: [{
              name: name, 
              age: age, 
              date: date, 
              idPassport: '002',
            },],
            success: true,
          })}
          rowKey="name" 
          params={{ keywords }}
          toolBarRender={(action) => [<Input.Search style={{width: 200,}} onSearch={(value) => setKeywords(value)}/>,]}
          pagination={{defaultCurrent: 10,}}
        />
      </div>
    </div>
  );
};
export default ProtableExample;