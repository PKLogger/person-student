import request from "./request";
import config from "./config";

let dataService = {
  displayProfile: (params) => {
    let url = "api/v1/sellers/profile"
    return request.post(params, url)
  },
  getListBanner: (params) => {
    let url = 'api/banner/get-all-banners';
    return request.post(params, url);
  },
};

export default dataService;
