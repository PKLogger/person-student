import config from './config';
// import api from '../component/Global/api';
const request = {
    get: (url) => {
        if (!url) url = 'api/rest'
        url = config.HOST + '/' + url
        return fetch(url)
            .catch(err => { console.log(err) })
            .then((response) => response.json())
    },
    post: async (data, url) => {
        url = config.HOST + '/' + url
        let headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjM4NTEwNTEsImlhdCI6MTYxNzg1MTA1MSwicGF5bG9hZCI6eyJzZWxsZXJfaWQiOiJzZWxsZXJjMTNlNTJhbXYwZHF2amIxNW9vZyIsInR5cGUiOiJhZ2VudCJ9fQ.KdSmKm9LWiLqg7NCa73djkuecyUP5oVXDaxW7ItV3n6siJwKA2btaWaS9SJLkVqQBqJmbwnHcPN7HXUlWdwdtzSuDCQ--d6TDj_41LHxRdQ9qa17Y4ogTvlHTC36Qvs5EA4bfj2LOCios50mV3ta_70wO6pMri8xy8JKHT8B9sM',
            'Accept-Language': 'vi'
        }
        process.env.NODE_ENV === 'development' && console.log(`\n %c-----------------------------[ POST ]-------------------------------------- \n [` + url + ` ] \n `, 'color:red;font-size:15px', headers, data, ' \n----------------------------------------------------------------------------- \n');
        try {
            let response = await fetch(url, {
                crossDomain: true,
                method: 'GET',
                headers,
                'Accept-Language': 'vi',
                // body: JSON.stringify(data),
            });
            console.log(response,'response')
            let rs = await response.json();
            process.env.NODE_ENV === 'development' && console.log(`\n %c-----------------------------[ RESPONSE ]------------------------------------ \n [` + url + ` ] \n `, 'color:green;font-size:15px', 'Data Post', data, `\n`, ' Respone  ', rs, ' \n----------------------------------------------------------------------------- \n');
            switch (response.status) {
                case 200: return rs
                // case 401: return logoutUser()
                default: {
                    console.log('err')
                    return rs
                    // throw (rs.message)
                }
            }
        } catch (error) {
            // console.log(error)
            // if (error.msg) {
            //     console.log(error.msg)
            // }
            throw error
        }
    }
}
// function logoutUser() {
//     // alert('Phiên làm việc của bạn đã hết hạn. vui lòng đăng nhập lại')
//     localStorage.clear('USER')
//     // window.location.href = "/";
// }
export default request