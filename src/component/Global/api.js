let history=null
let token = JSON.parse(localStorage.getItem('USER')) ? JSON.parse(localStorage.getItem('USER'))['token'] : ''
let store = null;
let api={
    setStore: (newStore) => {
        store = newStore;
    },
    getStore:()=>{
        return store
    },
    getToken:()=>{
        // return token
        return 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MjM4NTEwNTEsImlhdCI6MTYxNzg1MTA1MSwicGF5bG9hZCI6eyJzZWxsZXJfaWQiOiJzZWxsZXJjMTNlNTJhbXYwZHF2amIxNW9vZyIsInR5cGUiOiJhZ2VudCJ9fQ.KdSmKm9LWiLqg7NCa73djkuecyUP5oVXDaxW7ItV3n6siJwKA2btaWaS9SJLkVqQBqJmbwnHcPN7HXUlWdwdtzSuDCQ--d6TDj_41LHxRdQ9qa17Y4ogTvlHTC36Qvs5EA4bfj2LOCios50mV3ta_70wO6pMri8xy8JKHT8B9sM'
    },
    setHistory:(newHistory)=>{
        history=newHistory
    },
    getHistory:()=>{
        return history
    }
}

export default api;