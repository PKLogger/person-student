import React, { Component } from 'react';
import '../../../App.css';
import {SidebarData} from "./SidebarData";
function Sidebar(props) {
    return (
        <>
            
            <div className="c-sidebar">
                <ul className="c-sidebar-list">
                    {SidebarData.map((val, key) => {
                        return <li key={key} onClick={() => {window.location.pathname = val.link}}>
                            {" "}
                            <div id="icon">{val.icon}</div>{" "}
                            <div íd="title">{val.title}</div>
                        </li>;
                    })}
                </ul>
            </div>
        </>
    );
  }
export default Sidebar;
