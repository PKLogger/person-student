import React, { Component } from 'react';
import '../../../App.css';
import Sidebar from './Sidebar';
function AdminBootstrap(props) {
    return (
        <>
            {/* <div className="container-fluid" id="wrapper"> */}
            
                <div className="c-admin">
                    <div className="c-admin-body">
                        {/* Sidebar */}  <Sidebar/>
                    </div>
                </div>
            {/* </div> */}
        </>
    );
  }
export default AdminBootstrap;
