import React, { Component, useState } from 'react';
import '../../../App.css';
import {Layout, Avatar, Icon, Menu, Breadcrumb, Image, PageHeader, Button} from 'antd';
import Title from 'antd/lib/typography/Title';
import { AreaChartOutlined, BankOutlined, createFromIconfontCN, HomeOutlined, MenuFoldOutlined, MenuUnfoldOutlined, UserOutlined } from '@ant-design/icons';

import SubMenu from 'antd/lib/menu/SubMenu';

const { Header, Footer, Sider, Content } = Layout;

const IconFont = createFromIconfontCN({
    scriptUrl: [
      '//at.alicdn.com/t/font_1788044_0dwu4guekcwr.js', // icon-javascript, icon-java, icon-shoppingcart (overrided)
      '//at.alicdn.com/t/font_1788592_a5xf2bdic3u.js', // icon-shoppingcart, icon-python
    ],
  });
  
function AdminAntDesign(props) {
    const [collapsed, setCollapsed] = useState(false);

    const onCollapse = () => {
        setCollapsed(!collapsed);
    };
    return (
        <>
            <Layout>
                <Sider style={{overflow: 'auto',height: '100vh', width: '100vw',position: 'fixed', left: 0}} 
                collapsed={collapsed} onCollapse={onCollapse}>
                    <div className="c-logo"><a href="/">
                        <Image  style={{width: "170px", height: "32px"}}
                            src="http://static.ybox.vn/2017/9/12/7308165e-9771-11e7-831d-56c566ee3692.jpg"/>
                    </a> </div>
                    
                    <Menu defaultSelectedKeys={['Dashboard']} mode="inline" theme="dark">
                        <Menu.Item key="Dashboard">
                            <HomeOutlined />
                            Trang chủ
                        </Menu.Item>
                        <SubMenu title={<div><AreaChartOutlined /><span>Cinema</span></div>}>
                            <Menu.ItemGroup key="Cinema" title="Quản lý rạp">
                                <Menu.Item key="Cinema1">Rạp 1</Menu.Item>
                                <Menu.Item key="Cinema2">Rạp 2</Menu.Item>
                            </Menu.ItemGroup>
                        </SubMenu>
                        <SubMenu title={<div><IconFont type="icon-shoppingcart" /><span>Product</span></div>}>
                            <Menu.ItemGroup key="Product" title="Quản lý phim">
                                <Menu.Item key="Movie1">Movie 1</Menu.Item>
                                <Menu.Item key="Movie2">Movie 2</Menu.Item>
                            </Menu.ItemGroup>
                        </SubMenu>
                        <SubMenu title={<div><AreaChartOutlined /><span>Show</span></div>}>
                            <Menu.ItemGroup key="Show" title="Quản lý giờ chiếu">
                                <Menu.Item key="loaction1">Location</Menu.Item>
                                <Menu.Item key="loaction3">Location</Menu.Item>
                            </Menu.ItemGroup>
                        </SubMenu>
                        <SubMenu title={<div><BankOutlined /><span>Room</span></div>}>
                            <Menu.ItemGroup key="Room" title="Quản lý phòng">
                                <Menu.Item key="loaction2">Location</Menu.Item>
                                <Menu.Item key="loaction3">Location</Menu.Item>
                            </Menu.ItemGroup>
                        </SubMenu>
                    </Menu>
                </Sider>
                <Layout style={{ marginLeft: 200 }}>
                    <Header className="clearfix" style={{ padding: '12px 50px', background: 'white'}}>
                        <Title style={{ color: 'black', float: "left" }} level={3}>Trang chủ</Title>
                        <Avatar style={{ float: 'right'}} size="large" icon={<UserOutlined />}/>
                    </Header>
                    <Content style={{ padding: '0 50px', margin: '24px 16px 0', overflow: 'initial' }}>
                        <Breadcrumb style={{ margin: '16px 0'}}>
                            <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="site-layout-content">Content</div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>
                        ©2021 Team BETA
                    </Footer>
                </Layout>
            </Layout>, mountNote
        </>
    );
  }
export default AdminAntDesign;
