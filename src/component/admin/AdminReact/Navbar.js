import React, { useState } from 'react';
import '../../../App.css';
import './navbar.css';
import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import {Link} from "react-router-dom";
import { Sidebar } from 'ra-ui-materialui';
import {SidebarData} from "./SidebarData";
import {IconContext} from "react-icons";

function Navbar(props) {
    const [sidebar, setSidebar] = useState(false);
    const showSidebar = () => setSidebar(!sidebar);
    return (
        <>
        
           {/* <Admin dataProvider={restProvider('http://localhost:3000')}>
                <Resource name="post" list={PostList}/> 
            </Admin>  */}
            <IconContext.Provider value={{color: "fff"}}>
                <div className="navbar">
                    <Link to="#" className="menu-bars">
                        <FaIcons.FaBars onClick={showSidebar}/>
                    </Link>
                </div>
                <nav className={sidebar ? 'nav-menu-active' : 'nav-menu'}>
                    <ul className="nav-menu-items" onClick={showSidebar}>
                        <li className="navbar-toogle">
                            <Link to="#" className="menu-bars">
                                <AiIcons.AiOutlineClose/>
                            </Link>
                        </li>
                        {SidebarData.map((item, index) => {
                            return <li key={index} className={item.cName} >
                                <Link to={item.path}>
                                    {item.icon}
                                    <span>{item.title}</span>
                                </Link>
                            </li>;
                        })}
                    </ul>
                </nav>
            </IconContext.Provider>
        </>
    );
  }
export default Navbar;
