import React, { Component } from 'react';
import '../../../App.css';
import {Admin, Resource} from "react-admin";
import restProvider from "ra-data-simple-rest";
import PostList from "./PostList";
import NavBar from "./Navbar";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from "../../../pages/Home";
import Reports from "../../../pages/Reports";
import Products from "../../../pages/Products";
function AdminReact(props) {
    return (
        <>
           {/* <Admin dataProvider={restProvider('http://localhost:3000')}>
                <Resource name="post" list={PostList}/> 
            </Admin>  */}
            <Router>
                <NavBar/>
                <Switch>
                    <Route path='/' component={Home}/>
                    <Route path='/report' component={Reports}/>
                    <Route path='/products' component={Products}/>
                </Switch>
            </Router>
        </>
    );
  }
export default AdminReact;
