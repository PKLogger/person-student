import React, { Component } from 'react';
import '../../../App.css';
import {List, Datagrid, TextField, EditButton, DeleteButton} from "react-admin";
import restProvider from "ra-data-simple-rest";
const PostList = (props) => {
    return (
        <>
            <List {...props}>
                <Datagrid>
                    <TextField source="id"/>
                    <TextField source="title"/>
                    {/* <DataField source="publishedAt"/> */}
                </Datagrid>
            </List>
        </>
    );
  }
export default PostList;
